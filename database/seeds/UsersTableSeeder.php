<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $acc_role = Role::find(1);
    $exec_role = Role::find(2);

    $acc_user = new App\User;
    $acc_user->name = 'Ronnie G. Isurina';
    $acc_user->email = 'ronnie.isurina@gmail.com';
    $acc_user->password = bcrypt('ronnie');
    $acc_user->save();
    $acc_user->roles()->attach($acc_role);

    $exec_user = new App\User;
    $exec_user->name = 'Gemvir S. Isurina';
    $exec_user->email = 'gemvir.isurina@gmail.com';
    $exec_user->password = bcrypt('gemvir');
    $exec_user->save();
    $exec_user->roles()->attach($exec_role);
  }
}
