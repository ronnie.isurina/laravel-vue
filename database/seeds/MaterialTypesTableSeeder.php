<?php

use App\MaterialType as Type;
use Illuminate\Database\Seeder;

class MaterialTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [  'type' =>  'LABOR', 'description' => 'LABOR' ],
            [  'type' =>  'BREAKER', 'description' => 'BREAKER' ],
            [  'type' =>  'BRUSH', 'description' => 'BRUSH' ],
            [  'type' =>  'PESTICIDES', 'description' => 'PESTICIDES' ],
            [  'type' =>  'CEMENT', 'description' => 'CEMENT' ],
            [  'type' =>  'CHALK', 'description' => 'CHALK' ],
            [  'type' =>  'CHB', 'description' => 'CHB' ],
            [  'type' =>  'CLAMP', 'description' => 'CLAMP' ],
            [  'type' =>  'CONSUMABLE', 'description' => 'CONSUMABLE' ],
            [  'type' =>  'CUTTING DISK', 'description' => 'CUTTING DISK' ],
            [  'type' =>  'DOOR', 'description' => 'DOOR' ],
            [  'type' =>  'DOOR LOCK', 'description' => 'DOOR LOCK' ],
            [  'type' =>  'DRILLBIT', 'description' => 'DRILLBIT' ],
            [  'type' =>  'FENCING', 'description' => 'FENCING' ],
            [  'type' =>  'FIBER', 'description' => 'FIBER' ],
            [  'type' =>  'FITTING', 'description' => 'FITTING' ],
            [  'type' =>  'FIXED ASSET', 'description' => 'FIXED ASSET' ],
            [  'type' =>  'FIXTURE', 'description' => 'FIXTURE' ],
            [  'type' =>  'FLAT BAR', 'description' => 'FLAT BAR' ],
            [  'type' =>  'GRAVEL', 'description' => 'GRAVEL' ],
            [  'type' =>  'GROUT', 'description' => 'GROUT' ],
            [  'type' =>  'HOSE', 'description' => 'HOSE' ],
            [  'type' =>  'LUMBER', 'description' => 'LUMBER' ],
            [  'type' =>  'METAL', 'description' => 'METAL' ],
            [  'type' =>  'NAIL', 'description' => 'NAIL' ],
            [  'type' =>  'PAINT', 'description' => 'PAINT' ],
            [  'type' =>  'PANEL BOARD', 'description' => 'PANEL BOARD' ],
            [  'type' =>  'PIPE', 'description' => 'PIPE' ],
            [  'type' =>  'PROJECT', 'description' => 'PROJECT' ],
            [  'type' =>  'ROOFING', 'description' => 'ROOFING' ],
            [  'type' =>  'SAND', 'description' => 'SAND' ],
            [  'type' =>  'SCREW', 'description' => 'SCREW' ],
            [  'type' =>  'SEALANT', 'description' => 'SEALANT' ],
            [  'type' =>  'SEALER', 'description' => 'SEALER' ],
            [  'type' =>  'SKIMCOAT', 'description' => 'SKIMCOAT' ],
            [  'type' =>  'SOLVENT', 'description' => 'SOLVENT' ],
            [  'type' =>  'STAIR NOSING', 'description' => 'STAIR NOSING' ],
            [  'type' =>  'STEEL', 'description' => 'STEEL' ],
            [  'type' =>  'TAPE', 'description' => 'TAPE' ],
            [  'type' =>  'TILE TRIM', 'description' => 'TILE TRIM' ],
            [  'type' =>  'TILES', 'description' => 'TILES' ],
            [  'type' =>  'TOOLS', 'description' => 'TOOLS' ],
            [  'type' =>  'WATERPROFING', 'description' => 'WATERPROFING' ],
            [  'type' =>  'WIRE', 'description' => 'WIRE' ],
            [  'type' =>  'WOOD', 'description' => 'WOOD' ],
            [  'type' =>  'PANEL', 'description' => 'PANEL' ],
            [  'type' =>  'CONCRETE', 'description' => 'CONCRETE' ],
            [  'type' =>  'REBARS', 'description' => 'REBARS' ],
            [  'type' =>  'OUTSOURCE', 'description' => 'OUTSOURCE' ],
            [  'type' =>  'STONE', 'description' => 'STONE' ],
            [  'type' =>  'BOARD', 'description' => 'BOARD' ],
            [  'type' =>  'SET', 'description' => 'SET' ],
            [  'type' =>  'OTHERS', 'description' => 'OTHERS' ],
            [  'type' =>  'UPVC', 'description' => 'UPVC DOORS AND WINDOWS' ],
            [  'type' =>  'GRANITE', 'description' => 'GRANITE' ],
            [  'type' =>  'KITCHEN', 'description' => 'KITCHEN' ],
            [  'type' =>  'ELECTRICAL', 'description' => 'ELECTRICAL' ],
            [  'type' =>  'CLADDING', 'description' => 'CLADDING' ],
            [  'type' =>  'G.I', 'description' => 'G.I' ],
            [  'type' =>  'NEMA', 'description' => 'NEMA' ],
            [  'type' =>  'LIGHT', 'description' => 'LIGHT' ],
            [  'type' =>  'PLANTS', 'description' => 'PLANTS' ],
            [  'type' =>  'CONDUIT', 'description' => 'CONDUIT' ],
            [  'type' =>  'SHAFTING', 'description' => 'SHAFTING' ],
            [  'type' =>  'HINGES', 'description' => 'HINGES' ],
            [  'type' =>  'LADDER', 'description' => 'LADDER' ],
            [  'type' =>  'EDGING', 'description' => 'EDGING' ],
            [  'type' =>  'ADHESIVE', 'description' => 'ADHESIVE' ]
        ];

        Type::insert( $types );
    }
}