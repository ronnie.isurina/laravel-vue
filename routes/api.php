<?php

Route::group([ 'prefix' => 'v1' ], function() {
    Route::group([ 'prefix' => 'auth' ], function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });

    /** User **/
    Route::resource( 
        'user',  
        'UserController', 
        [ 'except' => [ 'edit', 'create' ],
        'parameters' => [ 'user' => 'id' ],
    ]);
    
    Route::group([ 'prefix' => 'user' ], function() {
        /* Reset User Password */
        Route::put('/{id}/passwordreset', [
            'uses' => 'UserController@password_reset',
            'as' => 'user.password_reset'
        ]);

        /* Add User role */
        Route::post('/{user_id}/role', [
            'uses' => 'UserController@store_role',
            'as' => 'user.store_role'
        ]);

        Route::delete('/{user_id}/role', [
            'uses' => 'UserController@destroy_role',
            'as' => 'user.destroy_role'
        ]);
    });

    /** Unit Of Measure **/
    Route::resource( 
        'unit-of-measure', 
        'UnitOfMeasureController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'unit-of-measure' => 'id' ],
        ]
    );

    /** Material Type **/
    Route::resource( 
        'material-type', 
        'MaterialTypeController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'material-type' => 'id' ],
        ]
    );
});