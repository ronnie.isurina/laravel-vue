<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $users = User::where('name', 'like', $search_key)
                        ->orWhere('email', 'like', $search_key)
                        ->orderBy('is_active', 'desc')
                        ->paginate($per_page);

        $users->withPath('');

        $response = [
            'message' => 'List of Users',
            'users' => $users
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Generate password
     * 
     * @return string
    **/
    private function generate_password() 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charLength = strlen($characters);
        $randomstr = '';

        for( $i = 0; $i < 6; $i++ ) {
            $randomstr .= $characters[rand(0, $charLength - 1)];
        }

        return $randomstr;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $name = $request->input('name');
        $generated_password = $this->generate_password();

        $user = new User;
        $user->name = $name;
        $user->email = $request->input('email');
        $user->password = bcrypt( $generated_password );

        $user->save();

        $response = [
            'msg' =>  $user->name . ' added as user!',
            'users' => $user,
            'password' => $generated_password
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            User::find($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'is_active' => 'boolean'
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $name = $request->input('name');
        $email = $request->input('email');

        $user = User::find($id);
        $user->name = $name;
        $user->email = $email;
        $user->is_active = $request->input('is_active');

        // Validate If name is exist
        $user_name_check = User::where('name', $name)->where('id', '!=', $id);
        if( $user_name_check->count() ){
            return response()->json( [ 'errors' => [
            'name' => ['The name has already been taken.']
            ] ], 200 );
        }

        // Validate If email is exist
        $user_email_check = User::where('email', $email)->where('id', '!=', $id);
        if( $user_email_check->count() ){
            return response()->json( [ 'errors' => [
            'email' => ['The email has already been taken.']
            ] ], 200 );
        }

        $user->save();

        $response = [
            'msg' =>  $user->name . ' was updates!',
            'user' => $user
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password_reset($id)
    {
        $new_password = $this->generate_password();

        $user = User::find($id);
        $user->password = bcrypt( $new_password );
        $user->save();

        $response = [
            'msg' =>  $user->name . ' new password was generated!',
            'user' => $user,
            'new_password' => $new_password, 
        ];

        return response()->json( $response, 200 );
    }

    public function store_role($user_id,Request $request) {
        $role = Role::find( $request->input('role_id') );
        $user = User::find( $user_id );

        $user->roles()->detach( $role );
        $user->roles()->attach( $role );

        $response = [
            'msg' =>  $user->name . ' access to ' . $role->name . ' was added!',
        ];

        return response()->json( $response, 200 );
    }

    public function destroy_role($user_id,Request $request) {
        $role = Role::find( $request->input('role_id') );
        $user = User::find( $user_id );

        $user->roles()->detach( $role );

        $response = [
            'msg' =>  $user->name . ' access to ' . $role->name . ' was deleted!',
        ];

        return response()->json( $response, 200 );
    }
}
