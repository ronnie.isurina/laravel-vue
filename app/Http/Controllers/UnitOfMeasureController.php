<?php

namespace App\Http\Controllers;

use App\UnitOfMeasure as Unit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UnitOfMeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $units = Unit::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $units->withPath('');

        $response = [
            'message' => 'List of UOM',
            'uoms' => $units
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:unit_of_measures',
            'base_qty' => 'required|integer',
            'base_unit' => 'required|max:50',
            'equal_qty' => 'required|integer',
            'equal_unit' => 'required|max:50'
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $unit = new Unit;
        $unit->code = $request->input('code');
        $unit->base_qty = $request->input('base_qty');
        $unit->base_unit = $request->input('base_unit');
        $unit->equal_qty = $request->input('equal_qty');
        $unit->equal_unit = $request->input('equal_unit');

        $unit->save();

        $response = [
            'message' =>  $unit->code . ' added to Unit of Measure!',
            'unit' => $unit,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'unit' => Unit::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'base_qty' => 'required|integer',
            'base_unit' => 'required|max:50',
            'equal_qty' => 'required|integer',
            'equal_unit' => 'required|max:50'
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $unit = Unit::find( $id );
        $unit->base_qty = $request->input('base_qty');
        $unit->base_unit = $request->input('base_unit');
        $unit->equal_qty = $request->input('equal_qty');
        $unit->equal_unit = $request->input('equal_unit');

        $unit->save();

        $response = [
            'message' =>  $unit->code . ' was updates!',
            'unit' => $unit
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $unit = Unit::find( $id );

        $unit->delete();

        $response = [
            'message' => $unit->unit . 'was delete!',
            'unit' => $unit
        ];

        return response()->json( $response, 200 );

    }
}
