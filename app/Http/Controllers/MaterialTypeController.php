<?php

namespace App\Http\Controllers;

use App\MaterialType as Type;
use Illuminate\Http\Request;

class MaterialTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $types = Type::where('type', 'like', $search_key)
                        ->orWhere('description', 'like', $search_key)
                        ->paginate($per_page);

        $types->withPath('');

        $response = [
            'message' => 'List of Material Type',
            'types' => $types
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'type' => 'required|string|max:255|unique:material_types',
            'description' => 'required|string|max:255',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $type = new Type;
        $type->type = $request->input('type');
        $type->description = $request->input('description');
        $type->save();

        $response = [
            'message' =>  $type->type . ' added to Material type!',
            'type' => $type
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'unit' => Type::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'type' => 'required|string',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $type = Type::find( $id );
        $type->type = $request->input('type');
        $type->description = $request->input('description');
        $type->save();

        $response = [
            'message' =>  $type->type . ' was updates!',
            'type' => $type
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $type = Type::find( $id );

        $type->delete();

        $response = [
            'message' => $type->type . 'was delete!',
            'type' => $type
        ];

        return response()->json( $response, 200 );

    }
}